<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 09.05.2018
 * Time: 06:26
 */


namespace Azizyus\LaravelImageManipulator;

use Azizyus\LaravelImageManipulator\RouteHelper\RouteHelper;
use Illuminate\Support\ServiceProvider;

class ImageManipulatorServiceProvider extends ServiceProvider
{


    public function boot()
    {

        $this->mergeConfigFrom(__DIR__.'/Config/image_manipulator.php','image_manipulator');
        $this->loadMigrationsFrom(__DIR__.'/Migrations');
        $this->loadViewsFrom(__DIR__."/views","ImageManipulator");


        $this->publishes([


            __DIR__. "/assets" => public_path("ImageManipulator/assets"),
            __DIR__.'/Config/image_manipulator.php' => config_path('image_manipulator.php'),
        //    __DIR__ . "/ImageManipulatorController.php" => app_path("Http/Controllers/ImageManipulatorController.php")


        ],"imageManipulator");


        RouteHelper::routes();
    }


    public function register()
    {





    }



}
