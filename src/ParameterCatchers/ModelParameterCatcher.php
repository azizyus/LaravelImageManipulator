<?php


namespace Azizyus\LaravelImageManipulator\ParameterCatchers;


use Illuminate\Http\Request;

class ModelParameterCatcher
{

    public function get(Request $request)
    {
        return $request->get('model',null);
    }

}
