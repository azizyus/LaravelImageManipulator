<?php


namespace Azizyus\LaravelImageManipulator\ParameterCatchers;


use Illuminate\Http\Request;

class LockedModelIdCatcher
{

    public function get(Request $request)
    {
        return $request->get('modelId');
    }

}
