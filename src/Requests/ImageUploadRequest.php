<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 10.05.2018
 * Time: 06:11
 */

namespace Azizyus\LaravelImageManipulator\Requests;
use Illuminate\Foundation\Http\FormRequest;
class ImageUploadRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image'=> 'required|max:10000|mimes:png,jpg,jpeg,gif,webp,svg',
        ];
    }


}
