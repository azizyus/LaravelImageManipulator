<?php

return [



    "authorizedModels" => [
        \App\User::class
    ],

    "uploadDir" => "uploads",



    //special images
    "specialImages"=>[

        \App\ImageDepot\MainThumbnail::class => [
            'key' => 'mainThumbnail',
            'title' => 'Thumbnail',
        ],
        \App\ImageDepot\MainCover::class => [
            'key' => 'mainCover',
            'title' => 'Cover',
        ]
    ],

    'specialImageNamespace' => \Azizyus\LaravelImageManipulator\Models\SpecialImage::class,
    'normalImageNamespace' => \Azizyus\LaravelImageManipulator\Models\Image::class,

    //every uploaded image has this if you provide right manipulator model
    "variations" => [
        \App\User::class => [
            \Azizyus\LaravelImageManipulator\Models\Image::class =>
                [
                    [
                        'key' => 'thumbnail',
                        'class' => \App\ImageDepot\Thumbnail::class,
                    ],
                    [
                        'key' => 'thumbnailWatermark',
                        'class' => \App\ImageDepot\WatermarkedThumbnail::class
                    ],
                ],
            \Azizyus\LaravelImageManipulator\Models\SpecialImage::class =>  [

                \App\ImageDepot\MainThumbnail::class => [
                    [
                        'key' => 'thumbnail',
                        'class' => \App\ImageDepot\Thumbnail::class
                    ],
                    [
                        'key' => 'thumbnailWatermark',
                        'class' => \App\ImageDepot\WatermarkedThumbnail::class
                    ]
                ],
                \App\ImageDepot\MainCover::class => [
                    [
                        'key' => 'cover',
                        'class' => \App\ImageDepot\MainCover::class
                    ]
                ]

            ]
        ]
    ],


    "cropSizeManipulators" => [

        \App\User::class => [

            [
                'title' => 'Thumbnail',
                'width' => 350,
                'height'=> 350
            ]

        ]

    ],


    'selectedImageColor' => '#007bff',

    'activateSpecialImageEdit' => true,
    'activateImageEdit' => true,

    'uploadRoute' => "images.upload",
    'cropRoute' => 'image.crop',
    'removeRoute' => 'image.remove',
    'sortRoute' => 'image.sort',

    //specifics
    'specificCropRoute' => 'image.specific.crop',
    'specificRemoveRoute' => 'image.specific.remove',
    'specificChangeRoute' => 'image.specific.change',

    'specialImageDefaultImagePath' => '/ImageManipulator/assets/images/default-image.jpg',


    "default" => \Azizyus\LaravelImageManipulator\Processor\DefaultImageProcessor::class,
    "dropzoneThumbnailDefault" => \Azizyus\LaravelImageManipulator\Processor\DropzoneThumbnail::class



];
