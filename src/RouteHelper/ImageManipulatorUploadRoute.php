<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 10.05.2018
 * Time: 06:19
 */
namespace  Azizyus\LaravelImageManipulator\RouteHelper;
use Illuminate\Support\Facades\Route;

class ImageManipulatorUploadRoute implements IRoute
{
    public static function route()
    {

        Route::group(['namespace'=>'Azizyus\LaravelImageManipulator\Controllers'],function(){

            Route::post("images/upload",["as"=>"images.upload","uses"=>"ImageManipulatorController@upload"]);
            Route::post("image/remove",["as"=>"image.remove","uses"=>"ImageManipulatorController@remove"]);
            Route::post("image/crop",["as"=>"image.crop","uses"=>"ImageManipulatorController@crop"]);
            Route::post("image/sort",["as"=>"image.sort","uses"=>"ImageManipulatorController@sort"]);
            Route::post("image/specific/change",["as"=>"image.specific.change","uses"=>"ImageManipulatorController@specificChange"]);
            Route::post("image/specific/crop",["as"=>"image.specific.crop","uses"=>"ImageManipulatorController@specificCrop"]);
            Route::post("image/specific/remove",["as"=>"image.specific.remove","uses"=>"ImageManipulatorController@specificRemove"]);

        });


    }
}
