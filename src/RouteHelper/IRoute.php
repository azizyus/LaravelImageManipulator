<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 10.05.2018
 * Time: 06:19
 */

namespace Azizyus\LaravelImageManipulator\RouteHelper;
use Illuminate\Support\Facades\Route;

interface IRoute
{


    public static function route();
}