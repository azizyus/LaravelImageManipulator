<?php


namespace Azizyus\LaravelImageManipulator\RouteHelper;


class RouteFactory
{
    public function make($route, $parameters = null)
    {
        return route($route,$parameters);
    }
}
