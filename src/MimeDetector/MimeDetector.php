<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 20.10.2018
 * Time: 19:31
 */

namespace Azizyus\LaravelImageManipulator\MimeDetector;
use Intervention\Image\Image;


/**
 * Class MimeDetector
 * @package Azizyus\LaravelImageManipulator\MimeDetector
 */
class MimeDetector
{

    /**
     * @var
     */
    public $searchingMime;


    /**
     * @var Image
     */
    public $image;

    /**
     * MimeDetector constructor.
     *
     * @param Image $image
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }


    /**
     * @return string
     */
    public function getMime()
    {
        return $this->image->mime();
    }


    /**
     * @return bool
     */
    public function hasMime()
    {
        if($this->getMime() == $this->searchingMime) return true;
        return false;
    }

    /**
     * @return mixed
     */
    public function getSearchingMime()
    {
        return $this->searchingMime;
    }

    /**
     * @param mixed $searchingMime
     */
    public function setSearchingMime($searchingMime): void
    {
        $this->searchingMime = $searchingMime;
    }





}