<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 09.05.2018
 * Time: 06:26
 */


namespace Azizyus\LaravelImageManipulator\Controllers;

use Azizyus\LaravelImageManipulator\ImageManipulatorHelper;
use Azizyus\LaravelImageManipulator\Models\ProcessedImage;
use Azizyus\LaravelImageManipulator\Models\SpecialImage;
use Azizyus\LaravelImageManipulator\ParameterCatchers\LockedModelIdCatcher;
use Azizyus\LaravelImageManipulator\ParameterCatchers\ModelParameterCatcher;
use Azizyus\LaravelImageManipulator\Processor\DropzoneThumbnail;
use Azizyus\LaravelImageManipulator\Requests\ImageUploadRequest;
use Azizyus\LaravelImageManipulator\Processor\DefaultImageProcessor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Azizyus\LaravelImageManipulator\Models\Image;
use Intervention\Image\Facades\Image as ImageInterventionFacade;

class ImageManipulatorController
{

    protected $modelParameterCatcher;
    protected $lockedModelIdCatcher;
    public function __construct()
    {
        $this->modelParameterCatcher = new ModelParameterCatcher();
        $this->lockedModelIdCatcher = new LockedModelIdCatcher();
    }

    public function test()
    {
        return "OK!";
    }

    public function sort(Request $request)
    {
        $model = $this->modelParameterCatcher->get($request);
        $helper = new ImageManipulatorHelper(Image::class,$model);
        $helper->setLockedModelId($this->lockedModelIdCatcher->get($request));
        $helper->updateSort($request);
        return 1;

    }


    public function upload(ImageUploadRequest $request)
    {
        $model = $this->modelParameterCatcher->get($request);
        $helper = new ImageManipulatorHelper(Image::class,$model);
        $helper->setLockedModelId($this->lockedModelIdCatcher->get($request));
        return $helper->upload($request);
    }
    public function crop(Request $request)
    {
        $model = $this->modelParameterCatcher->get($request);
       $helper = new ImageManipulatorHelper(Image::class,$model);
        $helper->setLockedModelId($this->lockedModelIdCatcher->get($request));
       return $helper->crop($request);

    }
    public function remove(Request $request)
    {
        $model = $this->modelParameterCatcher->get($request);
        $helper = new ImageManipulatorHelper(Image::class, $model);
        $helper->setLockedModelId($this->lockedModelIdCatcher->get($request));
        $helper->removeImage($request);
        return "true";
    }


    //SPECIFIC ROUTES
    public function specificChange(Request $request)
    {
        $model = $this->modelParameterCatcher->get($request);
        $helper = new ImageManipulatorHelper(Image::class,$model);
        $helper->setLockedModelId($this->lockedModelIdCatcher->get($request));
        return $helper->changeSpecialImage($request);
    }
    public function specificCrop(Request $request)
    {
        $model = $this->modelParameterCatcher->get($request);
        $helper = new ImageManipulatorHelper(SpecialImage::class,$model);
        $helper->setLockedModelId($this->lockedModelIdCatcher->get($request));
        $dump = $helper->cropSpecialImage($request);
        return $dump;
    }
    public function specificRemove(Request $request)
    {
        $model = $this->modelParameterCatcher->get($request);
        $helper = new ImageManipulatorHelper(SpecialImage::class,$model);
        $helper->setLockedModelId($this->lockedModelIdCatcher->get($request));
        return $helper->removeSpecific($request);
    }

}
