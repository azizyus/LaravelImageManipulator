<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 09.05.2018
 * Time: 08:51
 */


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{


    public function up()
    {
        Schema::create('images_x', function (Blueprint $table) {
            $table->increments('id');
            $table->text("image");
            $table->text("dropzoneThumbnail");
            $table->integer("order")->default(0);
            $table->integer("parentId");
            $table->text("manipulatorModel")->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('images_x');
    }

}
