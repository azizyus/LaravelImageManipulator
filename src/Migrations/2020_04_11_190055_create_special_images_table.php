<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('parentId');
            $table->string('key');
            $table->text('dropzonePreview');
            $table->text('image');
            $table->text('processedImage');
            $table->text("manipulatorModel")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_images');
    }
}
