<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 09.05.2018
 * Time: 06:31
 */

namespace Azizyus\LaravelImageManipulator;


use Azizyus\LaravelImageManipulator\Classes\DropzoneUploadResult;
use Azizyus\LaravelImageManipulator\Classes\ImageSavedAndProcessed;
use Azizyus\LaravelImageManipulator\Factory\DropzoneUploadResultFactory;
use Azizyus\LaravelImageManipulator\Interfaces\IProcessor;
use Azizyus\LaravelImageManipulator\MimeDetector\MimeDetector;
use Azizyus\LaravelImageManipulator\Models\ProcessedImage;
use Azizyus\LaravelImageManipulator\Models\SpecialImage;
use Azizyus\LaravelImageManipulator\Processor\DropzoneThumbnail;
use Azizyus\LaravelImageManipulator\RouteHelper\RouteFactory;
use Azizyus\LaravelImageManipulator\Variations\ConfigVariationParser\SpecialImageParser;
use Azizyus\LaravelImageManipulator\Variations\SpecialImageVariationHelper;
use Azizyus\LaravelImageManipulator\Variations\VariationHelper;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image as ImageInterventionFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Intervention\Image\Image as ImageInterventionInstance;

class ImageManipulatorHelper
{


    const _IMAGE_JPEG_MIME="image/jpeg";
    const _IMAGE_PNG_MIME="image/png";
    protected $imageModelNameSpace=null;
    protected $manipulatorModel;
    protected $routeFactory;
    protected $dropzoneUploadResultFactory;


    protected $lockModelId = null;

    public function getImageModelMameSpace()
    {
        return $this->imageModelNameSpace;
    }

    public function getManipulatorModel()
    {
        return $this->manipulatorModel;
    }

    public function setLockedModelId($override=null)
    {
        $this->lockModelId = $override;
    }

    protected function getLockedModelId(Request $request)
    {
        return $this->lockModelId;
    }


    public function __construct($imageModelNamespace, $manipulatorModel)
    {
        $this->setImageModelNamespace($imageModelNamespace);
        $this->setManipulatorModel($manipulatorModel);
        $this->routeFactory = new RouteFactory();
        $this->dropzoneUploadResultFactory = new DropzoneUploadResultFactory();
    }

    public function updateSort(Request $request)
    {
        $ids = $request->get('ids',[]);
        $images = $this->imageModelNameSpace::whereIn('id',$ids)->where('manipulatorModel',$this->manipulatorModel)->get();
        foreach ($ids as $key => $id)
        {
            $image = $images->where('id',$id)->first();
            $image->sort = $key;
            $image->save();
        }
    }

    public function getRouteFactory() : RouteFactory
    {
        return $this->routeFactory;
    }

    public function setRouteFactory(RouteFactory $routeFactory)
    {
        $this->routeFactory = $routeFactory;
    }

    public function setManipulatorModel(String $model)
    {
        $this->manipulatorModel = $model;
    }

    public function setImageModelNamespace(String $namespace)
    {
        $this->imageModelNameSpace = $namespace;
    }


    public function cropImage($cropBoxData, ImageInterventionInstance $image) : ImageInterventionInstance
    {


        $x = (int)$cropBoxData["x"];
        $y = (int)$cropBoxData["y"];
        $width = (int)$cropBoxData["width"];
        $height = (int)$cropBoxData["height"];
        $rotate = (int)$cropBoxData["rotate"];

      //  $image = $image->rotate($rotate);

        $image->crop($width,$height,$x,$y);



        return $image;


    }


    public function upload(Request $request)
    {

        $found = $this->manipulatorModel::where('id',$this->getLockedModelId($request))->first();
        $file = $request->file("image");
        $imageManipulatorImage = new $this->imageModelNameSpace();
        $imageManipulatorImage->parentId = $found->id;
        $imageManipulatorImage->isProcessable = true;
        $dropzoneUploadResult = null;
        if($file->getClientMimeType() == 'image/svg+xml')
        {
            $uniqueImageName = $this->getUniqueFileName(null,null,'svg');
            $this->saveFileToPath($file,$uniqueImageName);
            $dropzoneUploadResult = $this->dropzoneUploadResultFactory->make($uniqueImageName,$uniqueImageName,false);
            $imageManipulatorImage->isProcessable = false;
        }
        else $dropzoneUploadResult = $this->dropzoneProcessedUploadHandler(ImageInterventionFacade::make($file));
        //dropzone
        $imageManipulatorImage->image = $dropzoneUploadResult->originalImage->fileName;
        $imageManipulatorImage->dropzoneThumbnail = $dropzoneUploadResult->dropzoneThumbnail->fileName;
        $imageManipulatorImage->image = $dropzoneUploadResult->originalImage->fileName;
        $imageManipulatorImage->manipulatorModel = $this->manipulatorModel;

        $imageManipulatorImage->save();


        if($imageManipulatorImage->isProcessable)
            (new VariationHelper())->generateVariations($this->manipulatorModel,$file,$imageManipulatorImage,$this);

        return $dropzoneUploadResult->dump(["id"=>$imageManipulatorImage->id]);
    }

    public function removeImage(Request $request)
    {

        $imageId = $request->get('id');
        $modelId = $this->getLockedModelId($request);
        $model = $this->manipulatorModel;
        $image = $this->imageModelNameSpace::where('manipulatorModel',$model)->where('id',$imageId)
            ->where('parentId',$modelId)
            ->first();

        if($image)
        {

            File::delete($this->getImageSavePath($image->image));
            File::delete($this->getImageSavePath($image->dropzoneThumbnail));
            File::delete($this->getImageSavePath($image->processedImage));
            File::delete($this->getImageSavePath($image->sliderThumbnail));
            $image->delete();

        }



    }


    public function getImageSavePath($imageName)
    {



        return config("image_manipulator.uploadDir")."/$imageName";


    }

    /**
     * @param ImageInterventionInstance $processedImage
     * @param IProcessor $process
     * @return ImageInterventionInstance
     */

    public function processImageWithProcessor(ImageInterventionInstance $processedImage, IProcessor $process) : ImageInterventionInstance
    {


        $width   = $process->getCroppingWidth();
        $height  = $process->getCroppingHeight();



        $processedImage->encode($process->forcedFormat(),100);

        if($process->resize())
        {
            $processedImage->resize($width,$height,function($constraint){

                $constraint->aspectRatio();
              //  $constraint->upsize();

            });
        }


        //puts white background and inserts image to get absolute/fixed sized image
       if($process->withAbsoluteSize())
       {
           $background = Image::canvas($width,$height)->encode($process->forcedFormat(),100);
           $background->insert($processedImage,"center");
           return $background;

       }





        return $processedImage;

    }

    public function crop(Request $request)
    {
        $id = $request->id;
        $cropBoxData = $request->get('cropboxData');
        $model = $this->manipulatorModel;
        $modelId = $this->getLockedModelId($request);

        $foundImage = $this->imageModelNameSpace::where('id',$id)
        ->where('parentId',$modelId)
        ->where('manipulatorModel',$model)->first();

        if($foundImage)
        {

            $originalImageInstance = ImageInterventionFacade::make(public_path($this->getImageSavePath($foundImage->image)));
            $croppedImageInstance = $this->cropImage($cropBoxData, $originalImageInstance);
            $dropzoneThumbnailInstance = $this->processImageWithProcessor($croppedImageInstance,new DropzoneThumbnail());



            $croppedImageName = $this->getUniqueFileName('cropped_');
            $dropzoneImageName = $this->getUniqueFileName('dropzone_');

            $this->saveImageToPath($dropzoneThumbnailInstance,public_path($this->getImageSavePath($dropzoneImageName)));
            $this->saveImageToPath($croppedImageInstance,public_path($this->getImageSavePath($croppedImageName)));

            $dropzoneUploadResult = $this->dropzoneUploadResultFactory->make($foundImage->image,$dropzoneImageName);

            $foundImage->dropzoneThumbnail = $dropzoneImageName;

            $foundImage->save();

            (new VariationHelper())->generateVariations($this->manipulatorModel,public_path($this->getImageSavePath($croppedImageName)),$foundImage,$this);


            return $dropzoneUploadResult->dump([
                'id' => $request->get('id'),
            ]);
        }

    }

    public function dropzoneProcessedUploadHandler(ImageInterventionInstance $uploadedFile) : DropzoneUploadResult
    {
        $default = config("image_manipulator.default");
        $dropzone = config("image_manipulator.dropzoneThumbnailDefault");

        $originalFileName = $this->getUniqueFileName('original_');
        $dropzoneFileName = $this->getUniqueFileName('dropzoneThumbnail_');

        $this->saveUploadedFileAsImage($uploadedFile,new $default,$originalFileName);
        $this->saveUploadedFileAsImage($uploadedFile,new $dropzone,$dropzoneFileName);

        return $this->dropzoneUploadResultFactory->make($originalFileName,$dropzoneFileName);
    }


    public function checkNamespaceHasPermission(String $nameSpace)
    {
        $nameSpaceList = config("image_manipulator.authorizedModels");
        if(in_array($nameSpace,$nameSpaceList)) return true;
        else return false;
    }

    protected function getManipulator($specialImageKey)
    {
        foreach (config('image_manipulator.specialImages') as $key => $m)
        {
            if($m['key'] == $specialImageKey)
                return (new $key());
        }
    }




    public function changeSpecialImage(Request $request)
    {


        //MODEL NAMSPACE
        $imageManipulatorImageModel = $this->imageModelNameSpace;
        //MODEL NAMSPACE


        $selectedImageId = $request->selectedImageId;
        $model = $this->manipulatorModel;
        $modelId = $this->getLockedModelId($request);
        $specialImageKey = $request->get('specialImageKey');
//        $foundModel = $model::find($modelId);
        $foundImage = $imageManipulatorImageModel::where('parentId',$modelId)
        ->where('manipulatorModel',$model)
            ->where('id',$selectedImageId)
            ->first();

//        dd($request->all());
//        dd($foundImage);




        //if model found in db
        if($foundImage)
        {



//check is this an authorized namespace, ya know we dont want to let em reach to User or Admin classes
//            if($this->checkNamespaceHasPermission($model))
            if(true)
            {

                $specialImage = new SpecialImage();
                $specialImage->manipulatorModel = $model;


                $alreadyHasSpecialImage = SpecialImage::where('parentId',$modelId)
                ->where('manipulatorModel',$model)
                ->where('key',$specialImageKey)->first();

                if($alreadyHasSpecialImage)
                {
                    File::delete($this->getImageSavePath($alreadyHasSpecialImage->image == null ? "" : $alreadyHasSpecialImage->image));
                    File::delete($this->getImageSavePath($alreadyHasSpecialImage->dropzoneThumbnail == null ? "" : $alreadyHasSpecialImage->dropzoneThumbnail));
                    File::delete($this->getImageSavePath($alreadyHasSpecialImage->processedImage == null ? "" : $alreadyHasSpecialImage->processedImage));
                    $alreadyHasSpecialImage->delete();
                }


                if(!$foundImage->isProcessable)
                {
                    $originalImageName = $this->getUniqueFileName('original_',null,'svg');
                    $specialImage->processedImage  = $originalImageName;
                    $specialImage->dropzonePreview  = $originalImageName;
                    $specialImage->image  = $originalImageName;
                    $specialImage->parentId = $modelId;
                    $specialImage->key = $specialImageKey;
                    $specialImage->isProcessable = false;
                    $this->copyFileToSaveDirectory($foundImage->image,$originalImageName);
                    $dropzoneUploadResult = $this->dropzoneUploadResultFactory->make($originalImageName,$originalImageName,false);
                    $specialImage->save();
                    return $dropzoneUploadResult->dump(['id'=>$specialImage->id]);
                }



                $imageInterventionInstance = Image::make(public_path($this->getImageSavePath($foundImage->image)));

                //original image
                $originalImageName = $this->getUniqueFileName('original_');
                $this->saveImageToPath($imageInterventionInstance,public_path($this->getImageSavePath($originalImageName)));
                $specialImage->image = $originalImageName;

                //dropzone image
                $dropzoneImage = $this->processImageWithProcessor($imageInterventionInstance,new DropzoneThumbnail());
                $dropzoneImageName = $this->getUniqueFileName('dropzone_');
                $this->saveImageToPath($dropzoneImage,public_path($this->getImageSavePath($dropzoneImageName)));


                $manipulator = $this->getManipulator($specialImageKey);


                //processed image
                //probably it uses p
                $imageInterventionInstance = Image::make(public_path($this->getImageSavePath($foundImage->image)));
                $processedImage = $this->processImageWithProcessor($imageInterventionInstance,$manipulator);
                $processedImageName = $this->getUniqueFileName('processed_image');
                $this->saveImageToPath($processedImage,public_path($this->getImageSavePath($processedImageName)));

                $specialImage->dropzonePreview  = $dropzoneImageName;
                $specialImage->parentId = $modelId;
                $specialImage->key = $specialImageKey;

                $specialImage->processedImage = $processedImageName;
                $specialImage->save();


                $variationHelper = (new VariationHelper());
                $variationHelper->setVariationConfigParser(new SpecialImageParser());
                $variationHelper->variationConfigParser->setSpecialImageKey($specialImageKey);
                $variationHelper->generateVariations($this->getManipulatorModel(),$this->getImageSavePath($specialImage->image),$specialImage,$this);

                $d = new ImageSavedAndProcessed($dropzoneImageName,$this->getImageSavePath($dropzoneImageName));
                $o = new ImageSavedAndProcessed($originalImageName,$this->getImageSavePath($originalImageName));

                $dropzoneUploadResult = new DropzoneUploadResult($o,$d);
                return $dropzoneUploadResult->dump(["id"=>$specialImage->id]);
            }
            else
            {
                throw new \Exception("NOT AUTHORIZED NAMESPACE");
            }

        }
        else
        {
            throw new \Exception("MODEL NOT FOUND IN DB");
        }





    }

    public function cropSpecialImage(Request $request)
    {

        $modelId = $this->getLockedModelId($request);
        $id = $request->get('id');
        $model = $this->manipulatorModel;
        $key = $request->get('specialImageKey');
        $original = $this->imageModelNameSpace::where('id',$id)
            ->where('parentId',$modelId)
            ->where('manipulatorModel',$model)
            ->where('key',$key)
            ->first();

        $originalImageInstance = Image::make(public_path($this->getImageSavePath($original->image)));


        //cropped
        $cropped = $this->cropImage($request->get('cropboxData'),$originalImageInstance);
        $cropped = $this->processImageWithProcessor($cropped,$this->getManipulator($original->key));
        $croppedName = $this->getUniqueFileName('processed_');
        $this->saveImageToPath($cropped,public_path($this->getImageSavePath($croppedName)));
        $original->processedImage = $croppedName;

        //dropzone
        $dropzoneFromCropped = $this->processImageWithProcessor($cropped,new DropzoneThumbnail());
        $dropzoneName = $this->getUniqueFileName('dropzone_');
        $this->saveImageToPath($dropzoneFromCropped,public_path($this->getImageSavePath($dropzoneName)));
        $original->dropzonePreview = $dropzoneName;


        $original->save();


        $o = new ImageSavedAndProcessed(public_path($this->getImageSavePath($original->image)),$originalImageInstance,$original->image,$this->getImageSavePath($original->image));
        $d = new ImageSavedAndProcessed(public_path($this->getImageSavePath($dropzoneName)),$dropzoneFromCropped,$dropzoneName,$this->getImageSavePath($dropzoneName));
        $dropzoneUploadResult = new DropzoneUploadResult($o,$d);

        return $dropzoneUploadResult->dump([
            'id' => $request->get('id'),
        ]);



    }

    public function getUniqueFileName($prefix=null, ImageInterventionInstance $image=null,$defaultExtension='jpg'): String
    {


        if($image!=null)
        {

            $mimeDetector = new MimeDetector($image);

            $mimeDetector->setSearchingMime(self::_IMAGE_PNG_MIME);
            if($mimeDetector->hasMime()) $defaultExtension = "png";


            $mimeDetector->setSearchingMime(self::_IMAGE_JPEG_MIME);
            if($mimeDetector->hasMime()) $defaultExtension = "jpg";

        }



        $fileName="";

        if($prefix!=null) $fileName.=$prefix;
        $fileName .= uniqid().".$defaultExtension";


        return $fileName;

    }

    public function removeSpecific(Request $request)
    {

        $modelId = $this->getLockedModelId($request);
        $model = $this->manipulatorModel;
        $id = $request->get('id');
        $found = $this->imageModelNameSpace::where('parentId',$modelId)
            ->where('manipulatorModel',$model)
            ->where('id',$id)
            ->first();
        if($found)
        {


            File::delete($this->getImageSavePath($found->image));
            File::delete($this->getImageSavePath($found->droponePreview));
            File::delete($this->getImageSavePath($found->processedImage));
            $found->delete();
            return 1;
        }

        return 0;

    }

    public function saveImageToPath(\Intervention\Image\Image $image,String $absolutePath)
    {
        $image->save($absolutePath);
    }

    public function saveFileToPath(UploadedFile $file,String $fileName)
    {
        $file->move(public_path(config('image_manipulator.uploadDir')),$fileName);
    }

    public function copyFileToSaveDirectory($fileName, $newFileName)
    {
        File::copy(public_path(config('image_manipulator.uploadDir')).'/'.$fileName,public_path(config('image_manipulator.uploadDir')).'/'.$newFileName);
    }


    //default image will proccess here
    public function saveUploadedFileAsImage(ImageInterventionInstance $image, $imageProcessor, $fileName=null, $prefix=null) : ImageSavedAndProcessed
    {

        ## IMAGE INTERVENTION INSTANCE ##



        ## IMAGE INTERVENTION INSTANCE ##



        ## HANDLE FILE NAMING ##
        if($fileName==null)
        {
            $fileName = $this->getUniqueFileName($prefix,$image);
        }
        ## HANDLE FILE NAMING ##

        ##SAVE PATHS##
        $originalImageSavePath = $this->getImageSavePath($fileName);
        ##SAVE PATHS##





        ##ORIGINAL IMAGE##
        $image = $this->processImageWithProcessor($image,$imageProcessor);
        ##ORIGINAL IMAGE##



        ## IMAGE SAVE ##
        $this->saveImageToPath($image,$originalImageSavePath);
        ## IMAGE SAVE ##


        $imageReadyToSaveDB = new ImageSavedAndProcessed($fileName,asset($originalImageSavePath));

        return $imageReadyToSaveDB;


    }

}
