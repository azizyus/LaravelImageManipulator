<?php


namespace Azizyus\LaravelImageManipulator\Models;


trait HasImageRelation
{


    public function allImages()
    {
        return $this->hasMany(Image::class, 'parentId','id')->where('manipulatorModel',get_class($this))
            ->orderBy('sort','ASC')
            ;
    }

    public function specialImages()
    {
        return $this->hasMany(SpecialImage::class,'parentId','id')->where('manipulatorModel',get_class($this));
    }

    public function getSpecialOriginalImageByKey($key)
    {
        $found = $this->specialImages->where('key',$key)->first();
        if(!$found) return null;
        return $found->image;
    }

    public function getSpecialImageByKey($key)
    {
        $found = $this->specialImages->where('key',$key)->first();
        if(!$found) return null;
        return asset(config('image_manipulator.uploadDir').'/'.$found->processedImage);
    }

    public function getSpecialImageVariationByKey($key)
    {
        list($mainKey,$variationKey) = explode(".",$key);
        $found = $this->specialImages->where('key',$mainKey)->first();
        if($found) return $found->getVariation($variationKey);
        return null;
    }

}
