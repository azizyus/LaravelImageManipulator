<?php


namespace Azizyus\LaravelImageManipulator\Models;


class ImageRelations
{
    public static function make()
    {
        return ['allImages','allImages.variations','specialImages.variations'];
    }
}
