<?php


namespace Azizyus\LaravelImageManipulator\Models;

use Illuminate\Database\Eloquent\Model;


class ProcessedImage extends Model
{
        protected $table = 'processed_images';

        protected $fillable = [
            'parentId',
            'key',
            'image',
            'class'
        ];

}
