<?php


namespace Azizyus\LaravelImageManipulator\Models;


trait VariationTrait
{

    public function getOriginalImage()
    {
        return asset(config('image_manipulator.uploadDir').'/'.$this->image);
    }


    public function variations()
    {
        return $this->hasMany(ProcessedImage::class,'parentId','id')->Where('class',get_class($this));
    }

    public function getVariation($key=null)
    {
        if(!$this->isProcessable)
            return asset(config('image_manipulator.uploadDir').'/'.$this->image);
        else
        {
            $found = $this->variations->where('key',$key)->where('class',get_class($this))->first();
            if($found) return asset(config('image_manipulator.uploadDir').'/'.$found->image);
            return null;
        }
    }

}
