<?php


namespace Azizyus\LaravelImageManipulator\Models;


use Illuminate\Database\Eloquent\Model;

class SpecialImage extends Model
{

    use VariationTrait;

    protected $table = 'special_images';

    protected $fillable = [
        'parentId',
        'dropzonePreview',
        'image',
        'processedImage',
        'manipulatorModel',
        'key',
        'isProcessable',

    ];

}
