<?php


namespace Azizyus\LaravelImageManipulator\Models;

use Illuminate\Database\Eloquent\Model;


class Image extends Model
{
    use VariationTrait;

     protected $table = 'images_x';

     protected $fillable = [
         'image',
         'sliderThumbnail',
         'dropzoneThumbnail',
         'processedImage',
         'order',
         'parentId',
         'created_at',
         'updated_at',
         'deleted_at',
         'sort',
         'isProcessable',
     ];

}
