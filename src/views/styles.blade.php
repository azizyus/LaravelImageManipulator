<link rel="stylesheet" href="{{asset("ImageManipulator/assets/dropzone/dropzone5.1.1.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("ImageManipulator/assets/cropper/cropper.min.css")}}">



<style>
    #crop-modal
    {
        z-index: 999999 !important;
    }
    #change-custom-modal
    {
        z-index: 999999 !important;
    }
</style>

<style>

    .box
    {
        float:left;
    }
    .pointer
    {
        cursor: pointer;
        cursor:hand;
    }


    .custom-buttons
    {
        margin-bottom: 1%;
    }

</style>



{{--ICON FONT HEIGHT--}}
<style>



    .image-manipulator-container .fa
    {
        font-size: 1.5em;
    }
    .image-manipulator-container .ficon
    {
        font-size: 1.5em;
    }

</style>


{{--DROPZONE JS PREVIEW CSS--}}
<style>

    .dz-preview
    {
        background: rgba(255,255,255,0.8);
        position: relative;
        display: inline-block;
        margin: 17px;
        vertical-align: top;
        border: 1px solid #acacac;
        padding: 6px 6px 6px 6px;

    }


</style>

{{--THUMBNAIL CSS--}}
<style>



    .preview-thumbnails
    {

        height: 120px;
        width: 120px;
        object-fit: contain;
        object-position: top 75%;
        border: 1px solid rgba(0,0,0,0.3);
        margin-bottom: 10px;


    }
    .thumbnail_cover_label
    {
        position: absolute;
        left: 50%;
        transform: translate(-50%, 0);
        bottom:20%;
    }

    .blockUI
    {
        z-index: 1000000000 !important;
    }


</style>