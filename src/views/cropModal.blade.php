<div class="modal fade text-xs-left" id="crop-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-lg" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel16">Resim Düzenleme</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>

            </div>
            <div class="modal-body">




                <div class="card-body">
                    <div class="card-block">

                        <div class="row mb-1">
                            <div class="col-md-12 custom-buttons">
                                @foreach($cropSizes as $manipulator)

                                    <button data-has-aspect-ratio="true" data-width="{{$manipulator['width']}}" data-height="{{$manipulator['height']}}" class="btn-primary btn btn-sm cropper-fixed-crop">{{$manipulator['title']}}</button>

                                @endforeach

                                <button class="btn-primary btn btn-sm cropper-fixed-crop" >Free</button>
                            </div>
                            <div class="col-md-9">
                                <div class="img-container overflow-hidden" style="width: 750px; height: 500px;">
                                    <img class="main-demo-image img-fluid" src="{{asset("ImageManipulator/assets/images/default-image.jpg   ")}}" alt="Picture">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="docs-preview clearfix">
                                        <div class="img-preview preview-lg img-fluid"></div>
                                        <div class="img-preview preview-md img-fluid"></div>
                                        <div class="img-preview preview-sm img-fluid"></div>
                                        <div class="img-preview preview-xs img-fluid"></div>
                                    </div>
                                </div>

                                <!-- <h3 class="page-header">Data:</h3> -->
                                <div class="docs-data">
                                    {{--<fieldset class="form-group">--}}
                                    {{--<div class="input-group">--}}
                                    {{--<span class="input-group-addon">X</span>--}}
                                    {{--<input disabled="disabled" type="text" class="form-control main-demo-dataX" placeholder="x">--}}
                                    {{--<span class="input-group-addon">px</span>--}}
                                    {{--</div>--}}
                                    {{--</fieldset>--}}

                                    {{--<fieldset class="form-group">--}}
                                    {{--<div class="input-group">--}}
                                    {{--<span class="input-group-addon">Y</span>--}}
                                    {{--<input disabled="disabled" type="text" class="form-control main-demo-dataY" placeholder="y">--}}
                                    {{--<span class="input-group-addon">px</span>--}}
                                    {{--</div>--}}
                                    {{--</fieldset>--}}

{{--                                    <fieldset class="form-group">--}}
{{--                                        <div class="input-group">--}}
{{--                                            <span class="input-group-addon">Width</span>--}}
{{--                                            <input disabled="disabled" type="text" class="form-control main-demo-dataWidth" placeholder="width">--}}
{{--                                            <span class="input-group-addon">px</span>--}}
{{--                                        </div>--}}
{{--                                    </fieldset>--}}

{{--                                    <fieldset class="form-group">--}}
{{--                                        <div class="input-group">--}}
{{--                                            <span class="input-group-addon">Height</span>--}}
{{--                                            <input disabled="disabled" type="text" class="form-control main-demo-dataHeight" placeholder="height">--}}
{{--                                            <span class="input-group-addon">px</span>--}}
{{--                                        </div>--}}
{{--                                    </fieldset>--}}
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top: 5%;">
                            {{--<div class="col-md-2 col-sm-12">--}}
                            {{--<fieldset class="form-group text-xs-center">--}}
                            {{--<button class="btn btn-outline-blue rotate-m45-deg" type="button">-45&deg; Sola</button>--}}
                            {{--</fieldset>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-2 col-sm-12">--}}
                            {{--<fieldset class="form-group text-xs-center">--}}
                            {{--<button class="btn btn-outline-pink rotate-45-deg" type="button">45&deg; Sağa</button>--}}
                            {{--</fieldset>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-2 col-sm-12">--}}
                            {{--<fieldset class="form-group text-xs-center">--}}
                            {{--<button class="btn btn-outline-teal rotate-180-deg" type="button">180&deg; Çevir</button>--}}
                            {{--</fieldset>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-2 col-sm-12">--}}
                            {{--<fieldset class="form-group text-xs-center">--}}
                            {{--<button class="btn btn-outline-blue flip-horizontal" type="button" data-option="1">Yatay yansıma</button>--}}
                            {{--</fieldset>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-2 col-sm-12">--}}
                            {{--<fieldset class="form-group text-xs-center">--}}
                            {{--<button class="btn btn-outline-pink flip-vertical" type="button" data-option="1">Dikay yansıma</button>--}}
                            {{--</fieldset>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-2 col-sm-12">--}}
                            {{--<fieldset class="form-group text-xs-center">--}}
                            {{--<button class="btn btn-outline-teal zoom-in" type="button">Yakınlaştır</button>--}}
                            {{--</fieldset>--}}
                            {{--</div>--}}
                        </div>

                        {{--<div class="row mb-1">--}}
                        {{--<div class="col-md-4">--}}
                        {{--<fieldset class="form-group">--}}
                        {{--<div class="input-group">--}}
                        {{--<input type="text" class="form-control get-data">--}}
                        {{--<span class="input-group-btn">--}}
                        {{--<button class="btn btn-outline-blue get-data-btn" type="button">Get Data</button>--}}
                        {{--</span>--}}
                        {{--</div>--}}
                        {{--</fieldset>--}}
                        {{--</div>--}}

                        {{--<div class="col-md-4">--}}
                        {{--<fieldset class="form-group">--}}
                        {{--<div class="input-group">--}}
                        {{--<input type="text" class="form-control get-image-data">--}}
                        {{--<span class="input-group-btn">--}}
                        {{--<button class="btn btn-outline-pink get-image-data-btn" type="button">Get Image Data</button>--}}
                        {{--</span>--}}
                        {{--</div>--}}
                        {{--</fieldset>--}}
                        {{--</div>--}}

                        {{--<div class="col-md-4">--}}
                        {{--<fieldset class="form-group">--}}
                        {{--<div class="input-group">--}}
                        {{--<input type="text" class="form-control get-container-data">--}}
                        {{--<span class="input-group-btn">--}}
                        {{--<button class="btn btn-outline-teal get-container-data-btn" type="button">Get Container Data</button>--}}
                        {{--</span>--}}
                        {{--</div>--}}
                        {{--</fieldset>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="row">--}}
                        {{--<div class="col-md-6">--}}
                        {{--<fieldset class="form-group">--}}
                        {{--<div class="input-group">--}}
                        {{--<input type="text" class="form-control get-canvas-data">--}}
                        {{--<span class="input-group-btn">--}}
                        {{--<button class="btn btn-outline-blue get-canvas-data-btn" type="button">Get Canvas Data</button>--}}
                        {{--</span>--}}
                        {{--</div>--}}
                        {{--</fieldset>--}}
                        {{--</div>--}}

                        {{--<div class="col-md-6">--}}
                        {{--<fieldset class="form-group">--}}
                        {{--<div class="input-group">--}}
                        {{--<input type="text" class="form-control get-cropbox-data">--}}
                        {{--<span class="input-group-btn">--}}
                        {{--<button class="btn btn-outline-teal get-cropbox-data-btn" type="button">Get Crop Box Data</button>--}}
                        {{--</span>--}}
                        {{--</div>--}}
                        {{--</fieldset>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Kapat</button>
                <button data-preview-id="" onclick="saveImage(this)" type="button" id="save-image-button" class="btn btn-outline-primary">Kaydet</button>
            </div>
        </div>

    </div>
</div>
