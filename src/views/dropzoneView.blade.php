<div class="image-manipulator-container">

           <div class="row mb-1 mt-1">
               <div class="col-md-12">
                   <button class="delete-selected-images btn btn-md btn-primary">Seçili resimleri sil</button>
               </div>
           </div>
            <form action="{{$imageManipulatorHelper->getRouteFactory()->make(config('image_manipulator.uploadRoute'),["model"=>$imageManipulatorHelper->getManipulatorModel()])}}" method="POST" class="dropzone" id="my-dropzone"></form>

             <div class="row">
                 @foreach(config("image_manipulator.specialImages") as $key => $class)
                     <div  class='box custom-image-{{$class['key']}}'>
                         <div class="dz-preview dz-file-preview">

                             <div class="dz_image_preview  inner" data-thumbnail="http://placehold.it/60x60" data-original="http://placehold.it/60x60">
                                 <img id=""  class="preview-thumbnails"  data-dz-thumbnail src="{{\Azizyus\LaravelImageManipulator\Classes\SpecialImageGatherer::gatherDropzonePreview($class['key'],$specialImages)}}"  />
                             </div>
                             <div class="dz-details">

                             </div>

                             <div class="dz-error-message"><span data-dz-errormessage></span></div>
                             @if(config('image_manipulator.activateSpecialImageEdit'))
                                 <i image-is-processable="{{\Azizyus\LaravelImageManipulator\Classes\SpecialImageGatherer::gatherProcessable($class['key'],$specialImages)}}" data-parent-class="custom-image-{{$class['key']}}" data-image-key="{{$class['key']}}" data-image-crop-route="{{$imageManipulatorHelper->getRouteFactory()->make(config('image_manipulator.specificCropRoute'))}}" image-original-url="{{\Azizyus\LaravelImageManipulator\Classes\SpecialImageGatherer::gatherOriginalImage($class['key'],$specialImages)}}" image-id="{{\Azizyus\LaravelImageManipulator\Classes\SpecialImageGatherer::gatherImageId($class['key'],$specialImages)}}"  onclick="editImage(this,true)"  class="pull-left image-edit-button image-edit-button fa fa-crop edit_default_thumbnail"></i>
                             @endif
                             <i  image-id="{{\Azizyus\LaravelImageManipulator\Classes\SpecialImageGatherer::gatherImageId($class['key'],$specialImages)}}" data-remove-image-route="{{$imageManipulatorHelper->getRouteFactory()->make(config('image_manipulator.specificRemoveRoute'))}}" onclick="removeImage(this,false)"  class="image-remove-button pull-right fa fa-trash"></i>

                             <button data-image-key="{{$class['key']}}"  data-image-route="{{$imageManipulatorHelper->getRouteFactory()->make(config('image_manipulator.cropRoute'))}}"  data-parent-class="custom-image-{{$class['key']}}"  data-image-change-route="{{$imageManipulatorHelper->getRouteFactory()->make(config('image_manipulator.specificChangeRoute'))}}" class="btn btn-primary btn-sm change-current-extra-manipulator thumbnail_cover_label">{{$class['title']}}</button>

                         </div>
                     </div>
                 @endforeach
             </div>

</div>










