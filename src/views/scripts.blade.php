<script src="{{asset("ImageManipulator/assets/dropzone/dropzone5.1.1.js")}}"></script>
<script src="{{asset("ImageManipulator/assets/cropper/cropper.min.js")}}" type="text/javascript"></script>
<script src="{{asset("ImageManipulator/assets/dropzone/edit.js")}}"></script>

<script>


  //CROPPER
  function prepareOptions(width,height,hasAspectRatio) {

      if (hasAspectRatio) {
          return {
              viewMode: 2,
              setDragMode: "none",
              aspectRatio: width / height,
              preview: ".img-preview",
              crop: function (e) {
                  $dataX.val(Math.round(e.x)),
                      $dataY.val(Math.round(e.y)),
                      $dataHeight.val(height),
                      $dataWidth.val(width),
                      $dataRotate.val(e.rotate),
                      $dataScaleX.val(e.scaleX),
                      $dataScaleY.val(e.scaleY)
              }
          }
      }
      else {
          return {
              viewMode: 2,
              setDragMode: "none",
              //   aspectRatio: width / height,
              preview: ".img-preview",
              crop: function (e) {
                  $dataX.val(Math.round(e.x)),
                      $dataY.val(Math.round(e.y)),
                      $dataHeight.val(height),
                      $dataWidth.val(width),
                      $dataRotate.val(e.rotate),
                      $dataScaleX.val(e.scaleX),
                      $dataScaleY.val(e.scaleY)
              }
          }

      }
  }

  var originalImageUrl;
  var $image;
  $image = $(".main-demo-image"),
      $dataX = ($(".download"),
          $(".main-demo-dataX")),
      $dataY = $(".main-demo-dataY"),
      $dataHeight = $(".main-demo-dataHeight"),
      $dataWidth = $(".main-demo-dataWidth"),
      $dataRotate = $(".main-demo-dataRotate"),
      $dataScaleX = $(".main-demo-dataScaleX"),
      $dataScaleY = $(".main-demo-dataScaleY"),
  $(document).ready(function () {


  });


  //END CROPPER




    //DROPZONE



  function getImageList() {


      var editButtons = $(".edit-image-button");


      var images=[];

     // console.log(editButtons);


      editButtons.each(function (id,element) {


          var imageId = $(element).attr("image-id");
          var imageThumbnailUrl = $(element).attr("image-thumbnail-url");

          images.push( {

              id:imageId,
              imageThumbnailUrl:imageThumbnailUrl

          });
      });




        return images;


    //  console.log(images);



  }

  $(document).on("click",".delete-selected-images",function (){

      if(confirm("eminmisiniz?"))
          selectedImages.forEach(function(a){
              removeImage(a.removeButton,true,true);
          });

  });

  var selectedImages = [];
  $(document).on("click",".image-choose",function(){
      var selectedImageId = $(this).children('.remove-image-button').attr("image-id");
      var found = selectedImages.findIndex(function (a){
          if(a.id == selectedImageId)
              return true;
          return false;
      });
      if(found === -1)
      {
          $(this).css('background','{{config('image_manipulator.selectedImageColor')}}');
          selectedImages.push({
              id : selectedImageId,
              removeButton : $(this).children(".remove-image-button"),
          });
      }
      else
      {
          selectedImages.splice(found,1);
          $(this).css('background','white');
      }
  });

  //choosing image for thumbnail and etc etc
  $(document).on("click",".custom-image-option",function () {


      var selectedImageId = $(this).attr("data-custom-image-id");
      var parentClass = $(this).attr("data-parent-class");
      console.log("parent class is "+parentClass);

        $.ajax({


            "type":"POST",
            "url":lastCustomRoute,
            "data":{'specialImageKey':specialImageKey,"selectedImageId":selectedImageId,"_token":"{{csrf_token()}}","modelId":{{$modelId}},"model":"{!! str_replace("\\","\\\\",$imageManipulatorHelper->getManipulatorModel()) !!}"},
            success:function (response) {

                console.log("parent class before execute "+"."+parentClass);
                var imagePreviewElement =  $("."+parentClass);

                var previewThumbnail = imagePreviewElement.find(".preview-thumbnails");
                console.log(previewThumbnail);
                previewThumbnail.attr("src",response.dropzoneThumbnail);

                var dataHolderAndTrigger = imagePreviewElement.find(".change-current-extra-manipulator");
                var imageEditButton = imagePreviewElement.find(".image-edit-button");
                var imageRemoveButton = imagePreviewElement.find(".image-remove-button ");


                console.log("new "+parentClass+" is "+response.originalImage);
                console.log(response.isProcessable);
                imageEditButton.attr("image-original-url",response.originalImage);


                var p = 0;
                if(response.isProcessable) p = 1;

                imageEditButton.attr("image-is-processable",p);
                imagePreviewElement.attr("image-is-processable",p);
                dataHolderAndTrigger.attr("image-is-processable",p);


                imageEditButton.attr("image-id",response.id);
                imageRemoveButton.attr("image-id",response.id);
                dataHolderAndTrigger.attr("data-image-id",response.id);


                $("#change-custom-modal").modal("hide");

            },
            error:function () {

            }
        }).done(function () {



        });


  });


  var lastCustomRoute="";
  var specialImageKey = "";

  //open image list modal
  $(".change-current-extra-manipulator").on("click",function () {



      var images = getImageList();

      var imagesHtml="";

      lastCustomRoute = $(this).attr("data-image-change-route");
      specialImageKey = $(this).attr('data-image-key');
      var parentClass = $(this).attr("data-parent-class");
      // alert(lastCustomRoute);

      images.forEach(function (item) {


          imagesHtml+="<div data-parent-class='"+parentClass+"' data-custom-image-id='"+item.id+"'  class='pointer box pull-left custom-image-option'>\n" +
              "\n" +
              "\n" +
              "\n" +
              "\n" +
              "                                   <div class=\"dz-preview dz-file-preview\">\n" +
              "\n" +
              "\n" +
              "                                       <div class=\"dz_image_preview  inner\" >\n" +
              "                                           <img id=\"\"  class=\"preview-thumbnails\"  data-dz-thumbnail src='"+item.imageThumbnailUrl+"'  />\n" +
              "                                       </div>\n" +
              "                                       <div class=\"dz-details\">\n" +
              "\n" +
              "                                       </div>\n" +
              "\n" +
              "                                       <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n" +
              "\n" +
              "\n" +
              "                                   </div>\n" +
              "                               </div>   ";


      });

      $("#change-custom-modal").modal("show");


      $(".custom-modal-image-list").html("");
      $(".custom-modal-image-list").append(imagesHtml);



      console.log(images);



  });

    function saveImage(me) {



        var parentClass = $(me).attr("data-parent-class");
        var cropboxData = $image.cropper("getData");
        var id = $(me).attr("data-preview-id");

        var isCustom = $(me).attr("is-custom");

        var cropUrl = $(me).attr("data-image-crop-route");
        var specialImageKey = $(me).attr('data-image-key');

        console.log(cropboxData);
        console.log("CROP URL: "+cropUrl);

        var data = {"specialImageKey":specialImageKey,"cropboxData":cropboxData,"id":id,"selectedImageId":id,"_token":"{{csrf_token()}}","modelId":{{$modelId}},"model":"{!! str_replace("\\","\\\\",$imageManipulatorHelper->getManipulatorModel()) !!}"};

        $.ajax({


            "type":"POST",
            "url":cropUrl,
            "data":data,
            success:function () {
                console.log("image successfully ");
            },
            error:function () {

            },

        }).done(function (response) {


            if(isCustom!="true") //dropzone images
            {
            var imagePreviewElement =  $("*[image-id='"+response.id+"']");
            imagePreviewElement.attr("image-id",response.id);
            imagePreviewElement.attr("image-thumbnail-url",response.dropzoneThumbnail);
            imagePreviewElement.attr("image-original-url",response.originalImage);


            var imageElement=  $("*[image-id='"+response.id+"']").parent().children(".dz_image_preview").children(".preview-thumbnails");
            imageElement.attr("src",response.dropzoneThumbnail);

            }
            else
            {

                //custom images
                var imagePreviewElement =  $("."+parentClass);
                var previewThumbnail = imagePreviewElement.find(".preview-thumbnails");
                var imageEditButton =  imagePreviewElement.find(".image-edit-button");
                var imageRemoveButton =  imagePreviewElement.find(".image-remove-button");

                imageEditButton.attr("image-id",response.id);
                imageRemoveButton.attr("image-id",response.id);
                imageEditButton.attr("image-thumbnail-url",response.dropzoneThumbnail);
                imageEditButton.attr("image-original-url",response.originalImage);



                previewThumbnail.attr("src",response.dropzoneThumbnail);


            }
            console.log(imageElement);
            $("#crop-modal").modal("hide");


        });




    }

    function removeImage(me,removePreviewAfterDelete,skipConfirmation=false) {

      if(skipConfirmation || confirm("silmek istediğinize emin misiniz?"))
      {
          var id = $(me).attr("image-id");
          var removeRoute = $(me).attr('data-remove-image-route');

          console.log("DELETED IMAGE ID =  "+id);


          if(removeRoute == null) removeRoute = '{{$imageManipulatorHelper->getRouteFactory()->make(config('image_manipulator.removeRoute'))}}';

          $.ajax({


              "type":"POST",
              "url":removeRoute,
              "data":{"id":id,"_token":"{{csrf_token()}}","modelId":{{$modelId}},"model":"{!! str_replace("\\","\\\\",$imageManipulatorHelper->getManipulatorModel()) !!}"},
              success:function (data) {

                  console.log(data);

              },
              error:function () {

              }

          }).done(function () {



            if(removePreviewAfterDelete==true)  $(me).parent().remove();
              else
            {


                $(me).parent().find(".preview-thumbnails").attr("src","{{asset("ImageManipulator/assets/images/default-image.jpg")}}");

            }
          });
      }

    }


    function editImage(me,isCustom)
    {


        var cropUrl = $(me).attr("data-image-crop-route");

        var imageId  = $(me).attr("image-id");
        originalImageUrl = $(me).attr("image-original-url");
        specialImageKey = $(me).attr("data-image-key");
        var isProcessable = $(me).attr('image-is-processable');
        if(isProcessable == 0)
        {
            alert('not processable image format');
            return;
        }

        console.log("EDIT IMAGE ORIGINAL IMAGE URL: "+originalImageUrl );

         var parentClass = $(me).attr("data-parent-class");

        console.log("editing image id - "+imageId);
        console.log("editing image url - "+originalImageUrl);

        $image.cropper(prepareOptions(null,null,false)),
            $(".get-data-btn").on("click", function() {
                result = $image.cropper("getData"), $(".get-data").val(JSON.stringify(result))
            }),
            $(".get-image-data-btn").on("click", function() {
                result = $image.cropper("getImageData"), $(".get-image-data").val(JSON.stringify(result))
            }), $(".get-container-data-btn").on("click", function() {
            result = $image.cropper("getContainerData"), $(".get-container-data").val(JSON.stringify(result))
        }), $(".get-canvas-data-btn").on("click", function() {
            result = $image.cropper("getCanvasData"), $(".get-canvas-data").val(JSON.stringify(result))
        }), $(".get-cropbox-data-btn").on("click", function() {
            result = $image.cropper("getCropBoxData"), $(".get-cropbox-data").val(JSON.stringify(result))
        }), $(".download-cropped-canvas-btn").on("click", function() {}), $(".rotate-m45-deg").on("click", function() {
            $image.cropper("rotate", -45)
        }), $(".rotate-45-deg").on("click", function() {
            $image.cropper("rotate", 45)
        }), $(".rotate-180-deg").on("click", function() {
            $image.cropper("rotate", 180)
        }), $(".flip-horizontal").on("click", function() {
            var dataOption = $(this).data("option");
            $image.cropper("scaleX", -dataOption), $(this).data("option", -dataOption)
        }), $(".flip-vertical").on("click", function() {
            var dataOption = $(this).data("option");
            $image.cropper("scaleY", -dataOption), $(this).data("option", -dataOption)
        }), $(".zoom-in").on("click", function() {
            $image.cropper("zoom", .1)
        }),
            //SAĞDAKİ KÜÇÜK ÖNİZLEMELER
            $(".rotate-image").cropper({
                viewMode: 1,
                autoCropArea: .65,
                dragMode: "crop",
                restore: !1,
                zoomOnWheel: !1,
                built: function() {
//                $(".rotate-image").cropper("rotate", 45)
                }
            }), $(".scale-image").cropper({
            viewMode: 1,
            autoCropArea: .65,
            dragMode: "crop",
            restore: !1,
            zoomOnWheel: !1,
            built: function() {
                $(".scale-image").cropper("scale", -1)
            }
        })



         $image.cropper("replace",originalImageUrl);

         $(".cropper-crop-box").remove();
         $("#crop-modal").modal("show");
         var saveImageButton  = $("#save-image-button");
         saveImageButton.attr("data-preview-id",imageId);
         saveImageButton.attr("data-image-crop-route",cropUrl);
         saveImageButton.attr("data-parent-class",parentClass);
         saveImageButton.attr("is-custom",isCustom);
         saveImageButton.attr("data-image-key",specialImageKey);





    }

    Dropzone.autoDiscover = false;
    $(document).ready(function () {



        //clickin to size buttons in crop modal
        $(".cropper-fixed-crop").on("click",function () {


            var width  = $(this).attr("data-width");
            var height = $(this).attr("data-height");


            $image.cropper("destroy");

           if(width!=undefined && height!=undefined) $image.cropper(prepareOptions(width,height,true));
            else $image.cropper(prepareOptions(null,null,false));
            $image.cropper("replace",originalImageUrl);


        });




        var myDropzone =  $("#my-dropzone").dropzone({


            init:function()
            {


               @foreach($images as $image)

                    var mockFile = { name: "{{$image->dropzoneThumbnail}}", dropzoneThumbnail:"{{asset($imageManipulatorHelper->getImageSavePath($image->dropzoneThumbnail))}}",originalImage:"{{asset($imageManipulatorHelper->getImageSavePath($image->image))}}" , id:{{$image->id}},isProcessable:{{$image->isProcessable}}  };
                    {{--var mockFile = { name: "{{$image->dropzoneThumbnail}}",cropWidth:null,cropHeight:null,hasAspectRatio:false, dropzoneThumbnail:"{{asset($imageManipulatorHelper->getImageSavePath($image->dropzoneThumbnail))}}",originalImage:"{{asset($imageManipulatorHelper->getImageSavePath($image->image))}}" , id:{{$image->id}}  };--}}
                    // this.addFile.call(this, mockFile);
                    {{--this.options.thumbnail.call(this, mockFile, "{{asset($imageManipulatorHelper->getImageSavePath($image->dropzoneThumbnail))}}");--}}

                    this.emit("addedfile",mockFile);
                    this.emit("thumbnail",mockFile,"{{asset($imageManipulatorHelper->getImageSavePath($image->dropzoneThumbnail))}}");
                    this.emit("complete",mockFile);
                @endforeach

            },

            success:function(file,response){


                console.log("SUCCESSED FILE");


                var imagePreviewElement = $(file.previewElement.querySelectorAll("[data-dz-image-id]"));
                imagePreviewElement.attr("image-id",response.id);
                imagePreviewElement.attr("image-thumbnail-url",response.dropzoneThumbnail);
                imagePreviewElement.attr("image-original-url",response.originalImage);
                imagePreviewElement.attr("image-is-processable",response.isProcessable);

                // imagePreviewElement.attr("image-crop-width",response.cropWidth);
                // imagePreviewElement.attr("image-crop-height",response.cropHeight);
                // imagePreviewElement.attr("image-has-aspectratio",response.hasAspectRatio);



            },
            thumbnail: function(e,t){


                console.log("THUMBNAIL CREATING");


                //injects image id from db to
                var imagePreviewElement = $(e.previewElement.querySelectorAll("[data-dz-image-id]"));
                imagePreviewElement.attr("image-id",e.id);
                imagePreviewElement.attr("image-thumbnail-url",e.dropzoneThumbnail);
                imagePreviewElement.attr("image-original-url",e.originalImage);
                imagePreviewElement.attr("image-is-processable",e.isProcessable);

                for(e.previewElement.classList.remove("dz-file-preview"),
                        i=0, n=(r=e.previewElement.querySelectorAll("[data-dz-thumbnail]")).length;
                    i<n;i++)(s=r[i]).alt=e.name,s.src=t;

                return setTimeout(function(){

                    return e.previewElement.classList.add("dz-image-preview")
                },1)



            },
            sending:function(file, xhr, formData) {



                // Pass token. You can use the same method to pass any other values as well such as a id to associate the image with for example.
                formData.append("_token", "{{csrf_token()}}"); // Laravel expect the token post value to be named _token by default


                @foreach($extraData as $key => $data)

                formData.append("{{$key}}","{{$data}}");

                @endforeach

            },

            paramName: "image",
            parallelUploads:1,
            maxFilesize: 50,
            timeout: 3000000,
            thumbnailMethod:"contain",
            acceptedFiles:"image/*",
            dictDefaultMessage:"Fotoğraf Sürükle Bırak",
            previewTemplate:'' +
            '<div class="dz-preview dz-file-preview image-choose">\n  ' +

            '<div class="dz_image_previewe">'  +

                '<img class="preview-thumbnails" data-dz-thumbnail />'  +
            '</div>\n  '   +
                '<div class="dz-details">\n   '  +
            // '<div class="dz-size"><span data-dz-size></span></div>\n    '  +
            // '<div class="dz-filename"><span data-dz-name></span></div>\n  '  +
            '</div>\n  '  +
            '<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>\n '  +
            ' <div class="dz-error-message"><span data-dz-errormessage></span></div>\n  ' +
                    @if(config('image_manipulator.activateImageEdit'))
                        '<i  data-image-crop-route="{{$imageManipulatorHelper->getRouteFactory()->make('image.crop')}}" onclick="editImage(this,false)" data-dz-image-id    class="edit-image-button pull-left fa fa-crop"></i><i onclick="removeImage(this,true)" data-dz-image-id class="remove-image-button pull-right fa fa-trash"></i>\n  ' +
                    @endif
            '<div class="dz-success-mark">\n    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">\n      <title>Check</title>\n      <defs></defs>\n      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">\n        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>\n      </g>\n    </svg>\n  </div>\n  ' +
            '<div class="dz-error-mark">\n    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">\n      <title>Error</title>\n      <defs></defs>\n      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">\n        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">\n          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>\n        </g>\n      </g>\n    </svg>\n  </div>\n' +
            '</div>',


        });




    });
    //END DROPZONE




</script>


<script>
    $( function() {
        $( "#my-dropzone" ).sortable({
            update:function(){
                var idList = [];
               $('.edit-image-button').each(function(a,b,c){
                   // console.log(a,b,c);
                   idList.push($(b).attr('image-id'));
               });

                var sortRoute = '{{$imageManipulatorHelper->getRouteFactory()->make(config('image_manipulator.sortRoute'))}}';
                var data = {'ids':idList,'modelId':'{{$modelId}}','model':'{!! str_replace("\\","\\\\",$imageManipulatorHelper->getManipulatorModel()) !!}'};
                $.ajax({
                    'url':sortRoute,
                    'type':'POST',
                    data:data,
                    success:function(){
                        console.log('sort update success');
                    }
                });
            },
            change:function(){
                console.log('change');
            }
        });
        $( "#my-dropzone" ).disableSelection();

    } );
</script>
