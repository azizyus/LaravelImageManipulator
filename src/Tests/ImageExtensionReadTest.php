<?php

namespace Azizyus\LaravelImageManipulator\Tests;


use App\ImageManipulatorImage;
use Azizyus\LaravelImageManipulator\ImageManipulatorHelper;
use Intervention\Image\Facades\Image;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImageExtensionReadTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {



        $image = Image::make(__DIR__."/images/package1.png");
        $imageRealJpg = Image::make(__DIR__."/images/real_.jpg");
        $imageSameButJpg = Image::make(__DIR__."/images/package1_same_but_.jpg");
        $imageShadow = Image::make(__DIR__."/images/package1_shadow.png");

        if($image->mime()==ImageManipulatorHelper::_IMAGE_PNG_MIME) $this->assertTrue(true);
        else $this->assertTrue(false);



        if($imageRealJpg->mime() == ImageManipulatorHelper::_IMAGE_JPEG_MIME) $this->assertTrue(true);
        else $this->assertTrue(false);


        if($imageSameButJpg->mime() == ImageManipulatorHelper::_IMAGE_PNG_MIME) $this->assertTrue(true);
        else $this->assertTrue(false);

        if($imageShadow->mime() == ImageManipulatorHelper::_IMAGE_PNG_MIME) $this->assertTrue(true);
        else $this->assertTrue(false);


    }
}
