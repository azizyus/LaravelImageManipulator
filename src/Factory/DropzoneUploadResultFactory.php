<?php


namespace Azizyus\LaravelImageManipulator\Factory;


use Azizyus\LaravelImageManipulator\Classes\DropzoneUploadResult;
use Azizyus\LaravelImageManipulator\Classes\ImageSavedAndProcessed;
use Illuminate\Http\UploadedFile;

class DropzoneUploadResultFactory
{
    public function make(String $originalFileName,String $dropzoneThumbnailFileName,$isProcessable=true)
    {
        $o = new ImageSavedAndProcessed($originalFileName,asset(config('image_manipulator.uploadDir').'/'.$originalFileName));
        $d = new ImageSavedAndProcessed($dropzoneThumbnailFileName,asset(config('image_manipulator.uploadDir').'/'.$dropzoneThumbnailFileName));
        return new DropzoneUploadResult($o,$d,$isProcessable);
    }
}
