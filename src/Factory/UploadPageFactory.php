<?php


namespace Azizyus\LaravelImageManipulator\Factory;


use Azizyus\LaravelImageManipulator\ImageManipulatorHelper;
use Azizyus\LaravelImageManipulator\Variations\CropSizeConfigParser\CropSizeParser;

class UploadPageFactory
{

    public function makeCropSizeParser()
    {
        return new CropSizeParser();
    }

    public function make($modelInstance,String $imageModel,String $userModel=null,$cropSize=null)
    {
        return [
            'imageManipulatorHelper' => new ImageManipulatorHelper($imageModel,$userModel??get_class($modelInstance)),
            'cropSizes' => $cropSize??($this->makeCropSizeParser())->parse($userModel??get_class($modelInstance)),
            'modelInstance' => $modelInstance,
            'specialImages' => $modelInstance->specialImages,
            'images' => $modelInstance->allImages,
        ];
    }

}
