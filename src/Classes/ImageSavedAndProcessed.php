<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 10.05.2018
 * Time: 09:02
 */


namespace Azizyus\LaravelImageManipulator\Classes;

class ImageSavedAndProcessed
{

    public $downloadLink;
    public $fileName;
    public function __construct(String $fileName,String $downloadLink)
    {
        $this->downloadLink = $downloadLink;
        $this->fileName = $fileName;
    }

}
