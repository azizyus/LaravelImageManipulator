<?php


namespace Azizyus\LaravelImageManipulator\Classes;


use Illuminate\Database\Eloquent\Collection;

class SpecialImageGatherer
{

    public static function gatherDropzonePreview($key, Collection $images)
    {
        $found = static::find($key,$images);
        if($found) return asset(config('image_manipulator.uploadDir').'/'.$found->getAttribute('dropzonePreview'));
        return config('image_manipulator.specialImageDefaultImagePath');
    }

    public static function gatherOriginalImage($key, Collection $images)
    {
        $found = static::find($key,$images);
        if($found) return asset(config('image_manipulator.uploadDir').'/'.$found->getAttribute('image'));
    }

    public static function gatherImageId($key, Collection $images)
    {
        $found = static::find($key,$images);
        if($found) return $found->id;
    }

    public static function gatherProcessable($key,Collection $images)
    {
        $found = static::find($key,$images);
        if($found) return $found->isProcessable;
    }

    public static function find($key, Collection $collection)
    {
        return $collection->where('key',$key)->first();
    }
}
