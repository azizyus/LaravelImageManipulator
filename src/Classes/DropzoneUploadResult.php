<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 10.05.2018
 * Time: 10:24
 */

namespace Azizyus\LaravelImageManipulator\Classes;


class DropzoneUploadResult
{


    public $originalImage;
    public $dropzoneThumbnail;
    public $isProcessable;

    /**
     * DropzoneUploadResult constructor.
     * @param ImageSavedAndProcessed $originalImage
     * @param ImageSavedAndProcessed $dropzoneThumbnail
     */

    public function __construct(ImageSavedAndProcessed $originalImage,ImageSavedAndProcessed $dropzoneThumbnail,$isProcessable = true)
    {
        $this->originalImage = $originalImage;
        $this->dropzoneThumbnail = $dropzoneThumbnail;
        $this->isProcessable = $isProcessable;
    }

    public function dump($extraData=[]) : array
    {

        $data= array(
            "originalImage" => asset($this->originalImage->downloadLink),
            "dropzoneThumbnail" => asset($this->dropzoneThumbnail->downloadLink),
            'isProcessable' => $this->isProcessable,
        );

        $data = array_merge($extraData,$data);

        return  $data;

    }


}
