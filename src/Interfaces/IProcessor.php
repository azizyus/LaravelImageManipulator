<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 09.05.2018
 * Time: 09:43
 */



namespace Azizyus\LaravelImageManipulator\Interfaces;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Image;
use PhpParser\Node\Expr\Cast\Bool_;

interface IProcessor
{

    public function getWatermark();

    public function getCroppingHeight() : int;

    public function getCroppingWidth() :  int;

    public function withAbsoluteSize() : Bool;

    public function resize(): Bool;

    public function forcedFormat() : String;


}
