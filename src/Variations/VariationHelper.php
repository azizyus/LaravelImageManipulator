<?php


namespace Azizyus\LaravelImageManipulator\Variations;


use Azizyus\LaravelImageManipulator\ImageManipulatorHelper;
use Azizyus\LaravelImageManipulator\Models\ProcessedImage;
use Azizyus\LaravelImageManipulator\Variations\ConfigVariationParser\Parser;
use Intervention\Image\Facades\Image as ImageInterventionFacade;

class VariationHelper
{


    public $variationConfigParser;

    public function __construct()
    {
        $this->setVariationConfigParser(new Parser());
    }

    public function setVariationConfigParser(Parser $parser)
    {
        $this->variationConfigParser = $parser;
    }

    public function generateVariations($manipulatorModel, $filePath, $imageManipulatorImage, ImageManipulatorHelper $imageManipulatorHelper)
    {
        //variation init
        $namespace = get_class($imageManipulatorImage);
        $variations = $this->variationConfigParser->getVariations($manipulatorModel,$namespace);
        ProcessedImage::where('parentId',$imageManipulatorImage->id)
            ->where('class',get_class($imageManipulatorImage))
            ->delete();

        foreach ($variations as $v)
        {

            $variationImage = ImageInterventionFacade::make($filePath)->encode('png',90);
            $variationProcess = new $v['class'];

            $variationImage = $imageManipulatorHelper->processImageWithProcessor($variationImage,$variationProcess);
            if($variationProcess->getWatermark())
            {
                try
                {
                    $watermark = ImageInterventionFacade::make($variationProcess->getWatermark())
                        ->resize($variationImage->width()/3,$variationImage->height()/3, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });
                    $variationImage->insert($watermark,'center');
                }
                catch (\Exception $e)
                {
                    throw new \Exception("there is problem about your watermark, be sure it's readable and exist");
                }
            }

            $variationFileName = $imageManipulatorHelper->getUniqueFileName($v['key'].'_',null,$variationProcess->forcedFormat());
            $imageManipulatorHelper->saveImageToPath($variationImage,$imageManipulatorHelper->getImageSavePath($variationFileName));
            $p = ProcessedImage::make([
                'parentId' => $imageManipulatorImage->id,
                'key' => $v['key'],
                'image' => $variationFileName,
                'class' => get_class($imageManipulatorImage),
            ]);
            $p->save();
        }
    }
}
