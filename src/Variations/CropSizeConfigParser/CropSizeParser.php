<?php


namespace Azizyus\LaravelImageManipulator\Variations\CropSizeConfigParser;


class CropSizeParser
{

    public function parse(String $model)
    {
        return config('image_manipulator.cropSizeManipulators')[$model];
    }

}
