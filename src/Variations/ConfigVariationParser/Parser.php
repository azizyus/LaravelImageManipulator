<?php


namespace Azizyus\LaravelImageManipulator\Variations\ConfigVariationParser;


class Parser
{

    public function getVariations($manipulatorModel,$namespace)
    {
        return config('image_manipulator.variations')[$manipulatorModel][$namespace];
    }

}
