<?php


namespace Azizyus\LaravelImageManipulator\Variations\ConfigVariationParser;


class SpecialImageParser extends Parser
{

    public function getVariations($manipulatorModel,$namespace)
    {
        $foundSpecialImageModel = null;

        foreach (config('image_manipulator.specialImages') as $n => $x)
        {
            if($x['key'] == $this->specialImageKey){
                $foundSpecialImageModel = $n;
                break;
            }
        }

        return (config('image_manipulator.variations')[$manipulatorModel][$namespace][$foundSpecialImageModel]);
    }

    protected $specialImageKey = null;
    public function setSpecialImageKey($key)
    {
        $this->specialImageKey = $key;
    }

}
