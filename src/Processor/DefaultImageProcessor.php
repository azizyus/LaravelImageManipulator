<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 09.05.2018
 * Time: 10:14
 */

namespace  Azizyus\LaravelImageManipulator\Processor;
use Azizyus\LaravelImageManipulator\Interfaces\IProcessor;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Image;

class DefaultImageProcessor implements IProcessor
{


    public function getWatermark()
    {
        return null;
    }

    public  function getCropRoute(): String
    {
        return Route("image.crop");
    }

    public  function getCustomRoute(): String
    {
        return "";
    }

    public   function getId(): int
    {
        return 0;
    }

    public   function getTitle(): string
    {
        return "default";
    }

    public   function getColor(): string
    {
        return "";
    }

    public  function columnName(): String
    {
        return "";
    }

    public   function getCroppingHeight(): int
    {
        return 330;
    }

    public   function getCroppingWidth(): int
    {
        return 550;
    }

    public   function resize(): Bool
    {
        return false;
    }

    public  function withAbsoluteSize(): Bool
    {
        return false;
    }

    public function forcedFormat(): String
    {
        return 'jpg';
    }


}
