<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 10.05.2018
 * Time: 08:35
 */

namespace Azizyus\LaravelImageManipulator\Processor;


use Azizyus\LaravelImageManipulator\Interfaces\IProcessor;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Image;

class DropzoneThumbnail implements IProcessor
{

    public function getWatermark()
    {
        return null;
    }


    public  function getCropRoute(): String
    {
        return Route("image.crop");
    }


    public  function columnName(): String
    {
        return "";
    }

    public  function getCustomRoute(): String
    {
        return "";
    }

    public  function getId(): int
    {
        return 0;
    }

    public  function getTitle(): string
    {
        return "DropzoneThumbnail";
    }

    public  function getColor(): string
    {
        return "#000000";
    }

    public  function getCroppingHeight(): int
    {
        return 120;
    }

    public  function getCroppingWidth(): int
    {
        return 120;
    }
    public   function resize(): Bool
    {
        return true;
    }

    public  function withAbsoluteSize(): Bool
    {
        return true;
    }

    public function forcedFormat(): String
    {
        return 'jpg';
    }

}
